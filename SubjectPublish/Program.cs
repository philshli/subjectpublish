﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Subjects;
using System.Threading;

namespace SubjectPublish
{
    class Program
    {
        static Random fateGenerator = new Random((int) DateTime.Now.Ticks);

        static void Main(string[] args)
        {
            var isContinue = true;
            var queue = new BlockingCollection<string>();

            Task.Run(() =>
            {
                var value = queue.Take();
                while (isContinue)
                {
                    Console.WriteLine($"move file from A to B: {value}");
                    value = queue.Take();
                }
            });


            RetryAction<string> retryAction = new RetryAction<string>(new int[] { 3, 10, 30 });

            retryAction.Subscribe((IRetryParameter<string> value) =>
            {
                Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId}: copying file {value.Parameter}");

                Thread.Sleep(1000);
                if (!IsSuccessful())
                {
                    throw new Exception($"error: {value.Parameter}");
                }
                
                Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId}: copied file {value.Parameter}");
                queue.Add(value.Parameter);
            });

            for (int i = 1; i < 100; i++)
            {
                retryAction.Execute($"File: {i}");
                Thread.Sleep(1000);
            }

            //Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        static bool IsSuccessful()
        {
            int fateIndex = fateGenerator.Next(4);

            if (fateIndex%5 == 1)
            {
                return true;
            }

            return false;
        }
    }
}
