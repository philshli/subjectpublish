﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace SubjectPublish
{
    public class RetryAction<T>
    {
        public IEnumerable<ISubject<IRetryParameter<T>>> Subjects { get; private set; }
        public IObservable<IRetryParameter<T>> ActionStream { get; private set; }
        private ISubject<IRetryParameter<T>> TriggerSubject { get; set; } 

        public RetryAction(int[] delaySequenceInSecond)
        {
            this.ActionStream = new Subject<RetryParameter<T>>();
            var mutableSubjects = new Collection<ISubject<IRetryParameter<T>>>();

            this.TriggerSubject =  new Subject<IRetryParameter<T>>();
            mutableSubjects.Add(this.TriggerSubject);
            this.ActionStream = this.ActionStream.Merge(this.TriggerSubject.Delay(TimeSpan.Zero));

            for (int i = 0; i < delaySequenceInSecond.Length; i++)
            {
                var subject = new Subject<IRetryParameter<T>>();
                mutableSubjects.Add(subject);
                this.ActionStream = this.ActionStream.Merge(subject.Delay(TimeSpan.FromSeconds(delaySequenceInSecond[i])));
            }
            this.Subjects = mutableSubjects;
        }

        public void Subscribe(Action<IRetryParameter<T>> action)
        {
            this.ActionStream.Subscribe((IRetryParameter<T> value) =>
            {
                ISubject<IRetryParameter<T>> subject = null;
                try
                {
                    action.Invoke(value);
                }
                catch (Exception)
                {
                    if (!value.Enumerator.MoveNext())
                    {
                        Console.WriteLine($"can not make it work...{value.Parameter}");
                        return;
                    }
                    subject = value.Enumerator.Current;
                    subject.OnNext(value);
                }
            });
        }

        public void Execute(T parameter)
        {
            var value = new RetryParameter<T>(parameter, this.Subjects.GetEnumerator());
            if (!value.Enumerator.MoveNext()) return;
      
            this.TriggerSubject.OnNext(value);
        }
    }
}
