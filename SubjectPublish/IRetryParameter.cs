﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace SubjectPublish
{
    public interface IRetryParameter<T>
    {
        T Parameter { get; }
        IEnumerator<ISubject<IRetryParameter<T>>> Enumerator { get; } 
    }
}
