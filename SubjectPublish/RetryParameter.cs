﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace SubjectPublish
{
    public class RetryParameter<T>: IRetryParameter<T>
    {
        public T Parameter { get; private set; }
        public IEnumerator<ISubject<IRetryParameter<T>>> Enumerator { get; private set; }

        public RetryParameter(T parameter, IEnumerator<ISubject<IRetryParameter<T>>> subjectEnumerator)
        {
            this.Parameter = parameter;
            this.Enumerator = subjectEnumerator;
        } 
    }
}
